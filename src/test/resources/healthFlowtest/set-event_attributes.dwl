{
  "headers": {
    "user-agent": "PostmanRuntime/7.26.10",
    "accept": "*/*",
    "postman-token": "eea34c20-9a91-4461-8aa4-ec4718a199f7",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "scheme": "http",
  "method": "GET",
  "queryParams": {},
  "requestUri": "/api/v1/",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/",
  "listenerPath": "/api/v1/*",
  "relativePath": "/api/v1/",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/api/v1/",
  "rawRequestPath": "/api/v1/",
  "remoteAddress": "/127.0.0.1:57291",
  "requestPath": "/api/v1/"
}