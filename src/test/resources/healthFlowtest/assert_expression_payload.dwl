%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "message": "Success",
  "details": "The timestamp from the Salesforce Org is 2021-04-13T06:27:42.109"
})