{
  "headers": {
    "user-agent": "PostmanRuntime/7.26.10",
    "accept": "*/*",
    "postman-token": "81d5674e-9413-4469-a8aa-37d8ed26034f",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "scheme": "http",
  "method": "GET",
  "queryParams": {},
  "requestUri": "/api/v1/",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/",
  "listenerPath": "/api/v1/*",
  "relativePath": "/api/v1/",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/api/v1/",
  "rawRequestPath": "/api/v1/",
  "remoteAddress": "/127.0.0.1:58476",
  "requestPath": "/api/v1/"
}