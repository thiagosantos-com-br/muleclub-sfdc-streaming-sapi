%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "Email": "admin@thiagosantos.com.br",
  "FirstName": "Mr John",
  "LastName": "Santos",
  "Id": "00Q5e000001AZFKEA4",
  "MobilePhone": "1546444456"
})